#ifndef Monitor_HPP
#define Monitor_HPP

#include <semaphore.h>
#include <string>
#include <thread>
#include <chrono>
#include <iostream>
#include <unistd.h>
#include <math.h>

using namespace std;
using namespace std::chrono;

const long int MAXITER = 10000000;
const double CONSTANT = 1000000;

extern double totalEmission;
extern sem_t totalEmissionSem;

using namespace std;

struct CarData
{
    string entranceNode;
    long int entranceTime;
    string exitNode;
    long int exitTime;
    long int emission;
    long int totalEmission;
};

struct Car
{
    thread carThread;
    int p;
    int carNum;
    int pathNum;
    CarData* carData;
};

class Monitor
{
private:
    struct Condition
    {
        sem_t sem;
        int count;
    };
    
    void initCondition(Condition*);
    void signal(Condition*);
    void wait(Condition*);
    
    Condition* next;
    sem_t mutex;
    int common;
    struct Road
    {
        int h;
        string start;
        string end;
    };

    Road* road;

    

public:
    Monitor();
    void initRoad(int, string, string);
    string getStartPoint();
    string getEndPoint();
    void roadProcess(Car*);
    ~Monitor();
};

typedef struct Car Car;
typedef struct CarData CarData;


#endif