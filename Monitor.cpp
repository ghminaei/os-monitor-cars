#include "Monitor.hpp"

long int getNowMs()
{
    return (duration_cast< milliseconds >(
    system_clock::now().time_since_epoch())).count();
}

void Monitor::initCondition(Monitor::Condition *cond)
{
    cond->count = 0;
    sem_init(&cond->sem, 0, 1);
}

void Monitor::wait(Condition* cond)
{
    cond->count++;
    if (next->count > 0) {
        signal(next);
    } else {
        sem_post(&mutex);
    }
    sem_wait(&(cond->sem));
    cond->count++;
}

void Monitor::signal(Monitor::Condition* cond)
{
    if (cond->count > 0) {
        next->count++;
        sem_post(&(cond->sem));
        wait(next);
        next->count--;
    }
}

Monitor::Monitor()
{
    next = new Condition();
    sem_init(&mutex, 0, 1);
    initCondition(next);
}

void Monitor::roadProcess(Car* c)
{
    sem_wait(&mutex);
    c->carData->entranceNode = road->start;
    c->carData->entranceTime = getNowMs();
    double emission = 0;
    for (double k = 0; k < MAXITER; k++) {
        emission += floor(k/(CONSTANT*c->p*road->h));
    }
    c->carData->emission = emission;
    c->carData->exitNode = road->end;
    c->carData->exitTime = getNowMs();

    sem_wait(&totalEmissionSem);
    totalEmission += emission;
    c->carData->totalEmission += totalEmission;
    sem_post(&totalEmissionSem);


    if (next->count > 0) {
        signal(next);
    } else {
        sem_post(&mutex);
    }
}

void Monitor::initRoad(int h, string st, string en)
{
    road = new Road();
    road->h = h;
    road->start = st;
    road->end = en;
}

string Monitor::getStartPoint() { return road->start; }

string Monitor::getEndPoint() { return road->end; }

Monitor::~Monitor()
{
    sem_destroy(&mutex);
    sem_destroy(&(next->sem));
}