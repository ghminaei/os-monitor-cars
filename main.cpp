#include <iostream>
#include <vector>
#include <sstream>
#include "Monitor.hpp"
#include <unistd.h>
#include <fstream>

using namespace std;

double totalEmission;
sem_t totalEmissionSem;

void initCar(
    Car* c,
    int carNum,
    int pathNum
    ) {
    c->p = rand()%10 + 1;
    c->carNum = carNum;
    c->pathNum =pathNum;
    c->carData = new CarData();
}

void writeCarDataToFile(Car* c)
{
    ofstream carFile;
    carFile.open ("carData/" + to_string(c->pathNum) + "-" + to_string(c->carNum), ios_base::app);
    carFile << c->carData->entranceNode;
    carFile << ",";
    carFile << c->carData->entranceTime;
    carFile << ",";
    carFile << c->carData->exitNode;
    carFile << ",";
    carFile << c->carData->exitTime;
    carFile << ",";
    carFile << c->carData->emission;
    carFile << ",";
    carFile << c->carData->totalEmission;
    carFile << "\n";
    carFile.close();
}

Monitor* findMonitor(string start, string end, vector<Monitor*>& monitors)
{
    for (int i = 0; i < monitors.size(); i++) {
        if (monitors[i]->getStartPoint() == start &&
            monitors[i]->getEndPoint() == end) {
            return monitors[i];
        }
    }
    return NULL;
}

void getInp(vector<vector <string>>& pathParts, vector<vector <string>>& paths)
{
    string inp, word;
    vector<string> row; 
    while (cin >> inp) {
        if (inp[0] == '#')
            break;
        row.clear();
        stringstream s(inp);
        while (getline(s, word, '-')) {
            row.push_back(word);
        }
        pathParts.push_back(row);
    }

    while (cin >> inp) {
        row.clear();
        stringstream s(inp);
        while (getline(s, word, '-')) {
            row.push_back(word);
        }
        cin >> inp;
        row.push_back(inp);
        paths.push_back(row);
    }
}

void moveCarAlongTheRoad(Car* car, vector <string> road, vector<Monitor*> monitors)
{
    for (int nodeNum = 0; nodeNum < road.size()-2; nodeNum++) {
    Monitor* mon = findMonitor(road[nodeNum], road[nodeNum+1], monitors);
        if (mon) {
            mon->roadProcess(car);
            writeCarDataToFile(car);
        }
    }
}

int main()
{
    srand(time(0));
    totalEmission = 0;
    sem_init(&totalEmissionSem, 0, 1);
    vector<vector <string>> pathParts, paths;
    getInp(pathParts, paths);

    vector<Monitor*> monitors;
    Monitor* mon;
    for (int i = 0; i < pathParts.size(); i++) {
        mon = new Monitor();
        mon->initRoad(stoi(pathParts[i][2]), pathParts[i][0], pathParts[i][1]);
        monitors.push_back(mon);
    }

    vector<Car*> cars;
    int countCar = 0;
    for (int pathNum = 0; pathNum < paths.size(); pathNum++) {
        for (int carNum = 0; carNum < stoi(paths[pathNum][paths[pathNum].size()-1]); carNum++) {
            Car* car = new Car;
            initCar(car, countCar, pathNum);
            car->carThread = thread(moveCarAlongTheRoad, car, paths[pathNum], monitors);
            countCar++;
            cars.push_back(car);
        }
    }

    for (int i = 0; i < cars.size(); i++) {
        cars[i]->carThread.join();
    }

    sem_destroy(&totalEmissionSem);
    return 0;
}
