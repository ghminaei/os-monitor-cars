CC=g++
CF=-std=c++11 

all: simulation
		make simulation
		make clean

simulation: main.o Monitor.o
	$(CC) $(CF) main.o Monitor.o -lpthread -lrt -o simulation

main.o: main.cpp
	$(CC) $(CF) -c main.cpp

Monitor.o: Monitor.cpp
	$(CC) $(CF) -c Monitor.cpp

clean:
	rm *.o
